Vscode settings
==========

Preferences for vscode

Settings
-------

> settings.json

For linux put in: **$HOME/.config/Code/User/settings.json**


Key bindings
-------

> keybindings.json

For linux put in: **$HOME/.config/Code/User/keybindings.json**
